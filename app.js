const
    express     = require('express'),
    path        = require('path'),
    bodyParser  = require('body-parser'),
    fs          = require('fs'),
    engines     = require('consolidate'),
    app         = express();

app.use(express.static(path.join(__dirname,'public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.set('views', __dirname + '/public');
app.engine('html', engines.mustache);
app.set('view engine','html');

app.post('/post/:cat',function(req,res){
    console.log(req.body);
    console.log(req.params.cat);
    
    var content = JSON.stringify(req.body);
    
    fs.writeFile(__dirname + '/public/data/' + req.params.cat + '.json', content, function(err){
        if(err) throw err;
    console.log('saved');
    })
    res.setHeader('Content-Type', 'application/json');    
    res.end(content);
});
app.get('/newsite/:prod/:img',function(req,res){
    res.render('newsite.html',{
        prod : req.params.prod,
        img  : req.params.img
    });


});

app.listen(3000);
console.log('server listening on 3000');