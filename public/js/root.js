var root = angular.module('root',['ngTagsInput']);

root.controller('showData',function($scope,$http,$location){

     $scope.needJson = false;
     $scope.category = ['slim','wrinkles','hair','potency'];

     $scope.data = {};
     $scope.response = {};
     $scope.opis = 'Tutaj jakiś opis produktu';
     $scope.show = function(cat) {
         $scope.url = $location.absUrl();

         $http.get($scope.url + 'data/' + $scope.category[cat] + '.json')
             .success(function(data) {
               $scope.json = data;

               $scope.imgProd = function(productName) {
                if (productName == "Kankusta Duo") {
                    return 'kankusta-duo.png'
                }
                else if (productName == "Green Coffee") {
                    return 'green-coffee.jpg'
                }
                else {
                    return 'default-image.jpg';
                }
               }
             })
             .error(function(err) {
               return err;
             });

     }
     $scope.save = function(cat) {

         $http({
             url: '/post/'+cat,
             method: 'POST',
             data: $scope.json,
             headers: {
                 'Content-Type': 'application/json'
             }
         }).success(function(response) { console.log(response);
             $scope.response.data = response;
         });

     };
     $scope.loadTags = function(query) {
       return $http.get($scope.url + 'data/tags_slim.json');
     };


});
