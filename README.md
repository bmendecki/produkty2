### Przeglądarka produktów ###

Funkcje: 

- pokazuje tabelę produkty vs rynki vs kategorie
- pozwala na edycję i zapis 

W przyszłości:

- usuwanie i dodawanie produktów
- wybór produktów z listy
- podpięcie MongoDb + REST do niej

Technologia:
- angularjs
- nodejs

Wymagania:
 
- NodeJs 
- wolny port 3000 lub inny